﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DraggableObject : MonoBehaviour {

    /* 
     * Y Position to move to when picked up
     */
    abstract public int PlaneY
    {
        get;
    }

    /*
     * Allows the object to "ghost" behind the cursor, the higher the number the slower the delay.
     */
    abstract public int DragSpeed
    {
        get;
    }

    /*
     * Should be set to false if any physics calculations are run after being dropped, eg: gravity/rigidbody force
     */
    abstract public bool ShouldKeepLerpingWhenDropped
    {
        get;
    }

    /*
     * 
     */
    private Vector3 moveTowardsVector = Vector3.zero;

    private bool isPickedUp = false;

    public void MoveTo(Vector3 dir)
    {
        moveTowardsVector = dir;
    }

    public virtual void Update()
    {

        if (moveTowardsVector == Vector3.zero)
            return;

        if(DragSpeed <= 0)
        {
            transform.position = moveTowardsVector;
        }
        else
        {
            if (isPickedUp)
            {
                transform.position = Vector3.Lerp(transform.position, moveTowardsVector, Time.deltaTime * DragSpeed);
            }
            else if( !isPickedUp && ShouldKeepLerpingWhenDropped)
            {
                transform.position = Vector3.Lerp(transform.position, moveTowardsVector, Time.deltaTime * DragSpeed);
            }
        }

    }
    
    public virtual void OnPickup(){
        isPickedUp = true;
    }

    public virtual void Drop() {
        isPickedUp = false;
    }

    public virtual bool CanBeDropped()
    {
        return true;
    }

    public virtual bool CanBePickedUp()
    {
        return true;
    }

}
