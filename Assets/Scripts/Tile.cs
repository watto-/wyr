﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {
    public static List<Tile> tiles = new List<Tile>();

    public static List<Tile> activeTiles = new List<Tile>();
    //Starting from 0 to 16 if there's 16 tiles, etc
    public int index;

    Color defaultColor;

    public static bool isTileActive(Tile tile)
    {
        foreach(Tile theTile in activeTiles){
            if (theTile == tile)
                return true;
        }

        return false;
    }
    public static Tile getTile(int index)
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            if (tiles[i].index == index)
                return tiles[i];
        }

        return null;
    }

    public static void clearActiveTiles()
    {
        //Set them back to their default colors
        foreach(Tile tile in activeTiles)
        {
            tile.GetComponent<Renderer>().material.color = tile.defaultColor;
        }
        activeTiles.Clear();
    }

    public void setActive()
    {
        this.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
        activeTiles.Add(this);
    }
	//All the transform spots available on this tile.
	public TileSpot[] locations;

	public Character[] OnThisTile {
		get {
            List<Character> temp = new List<Character>();

            foreach(TileSpot spot in locations)
            {
                if(spot.OccupyingPlayer != null)
                {
                    temp.Add(spot.OccupyingPlayer);
                }
            }

            return temp.ToArray();
        } //Loop through locations and return every player in it
	}
	// Use this for initialization
	void Start () {
		tiles.Add(this);

		if( this.index % 2 == 0 ){
			Renderer rend = GetComponent<Renderer>();
            rend.material.color = Color.black;
            defaultColor = Color.black;
        }
        else
        {
            defaultColor = Color.white;
        }
	}
	
	public TileSpot GetNextAvailableSpot(){
		foreach(TileSpot spot in locations){
			if(spot.OccupyingPlayer == null)
				return spot;
		}
		return null;
	}

	public bool isPlayerOccupyingSpot(Character player) {
		foreach(TileSpot spot in locations){
			if( spot.OccupyingPlayer == player )
				return true;
		}

		return false;
	}

	public void RemovePlayerFromSpot(Character player){
		foreach(TileSpot spot in locations){
			if(spot.OccupyingPlayer == player)
				spot.OccupyingPlayer = null;
		}
	}

    public bool MovePlayerToAvailableSpot(Character character)
    {
        TileSpot spot = GetNextAvailableSpot();

        if (spot == null)
        {
            return false;
        }

        if (character.CurrentTile)
            character.CurrentTile.RemovePlayerFromSpot(character);

        //Remove them from their current tile
        character.MoveTo(spot.SlotPosition);
        spot.OccupyingPlayer = character;
        character.CurrentTile = this;
        return true;
    }

    /*
        Checks number of characters on this tile vs how many are allowed, returns true if there is space for a player 
    */
    public bool hasAvailableSpot()
    {
        return OnThisTile.Length < locations.Length - 1;
    }
}
