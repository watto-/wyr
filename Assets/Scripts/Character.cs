﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : DraggableObject {


    /* Tile the player is currently on */
	private Tile currentTile;


    private void Start()
    {
        Tile tile = Tile.getTile(0);
        if (tile != null)
        {
            TileSpot spot = tile.GetNextAvailableSpot();

            if (spot != null)
            {
                this.MoveTo(spot.SlotPosition);
                spot.OccupyingPlayer = this;
                this.currentTile = tile;
            }
        }

        RollDie();
    }
    public Tile CurrentTile {
		get { return this.currentTile;}
		set { this.currentTile = value; }
	}

    public override void Drop()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 10))
        {
            //Checks if they are on top of a Tile, if they are check it's got a spot available
            if (hit.collider.tag == "Tile")
            {
                Tile tile = hit.collider.GetComponent<Tile>();
                if (tile != null)
                {
                    //Player is already stored in a spot, remove them from it
                    if (this.CurrentTile)
                        CurrentTile.RemovePlayerFromSpot(this);

                    TileSpot spot = tile.GetNextAvailableSpot();

                    if (spot != null)
                    {
                        this.MoveTo(spot.SlotPosition);
                        spot.OccupyingPlayer = this;
                        this.currentTile = tile;
                        RollDie();
                    }
                }
            }

        }
    }


    /* Returning False will return the object to it's initial place, returning true will leave it where it
       it was dropped, if you'd like to move it to a new location after being dropped override the Drop() function 
    */
    public override bool CanBeDropped()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, Vector3.down, out hit, 10)){
            /* Characters can only be dropped on valid tiles */
            if (hit.collider.tag == "Tile")
            {
                Tile tile = hit.collider.GetComponent<Tile>();
                if(tile != null)
                {
                    //If the player is not already on this Tile and there is a spot available then return true
                    return (!tile.isPlayerOccupyingSpot(this) && tile.hasAvailableSpot() && Tile.isTileActive(tile));
                }
            }
        }

        //Return false
        return false;
    }
    
    public override void Update () {
        base.Update();
    }



    public void RollDie()
    {
        var diceRoll = Random.Range(1, 6);

        if (!CurrentTile)
        {
            Debug.Log("Set it to a tile dickhead");
            return;
        }

        
        Tile.clearActiveTiles();

        var forwardTile = CurrentTile.index + diceRoll;
        var backTile = CurrentTile.index - diceRoll;

        if (forwardTile <= 63)
        {
            Tile tile = Tile.getTile(forwardTile);
            tile.setActive();
        }

        if (backTile >= 0)
        {
            Tile tile = Tile.getTile(backTile);
            tile.setActive();
        }
        //Tile.tiles
    }



    public override int PlaneY
    {
        get
        {
            return 1;
        }
    }

    public override int DragSpeed
    {
        get
        {
            return 10;
        }
    }

    public override bool ShouldKeepLerpingWhenDropped
    {
        get
        {
            return true;
        }
    }

}
