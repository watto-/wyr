﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : DraggableObject {

   
    Rigidbody rigidbody;

    // Use this for initialization
    void Start()
    {
        this.rigidbody = GetComponent<Rigidbody>();

        if (rigidbody == null)
        {
            Debug.LogError("No rigidbody component on Dice");
            return;
        }

        rigidbody.isKinematic = true;
    }

    public override void OnPickup()
    {
        rigidbody.isKinematic = true;
        base.OnPickup();
    }


    public override void Drop()
    {
        rigidbody.isKinematic = false;
        base.Drop();
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
    }
    int Value
    {
        get {
            if (Vector3.Dot(transform.right, Vector3.up) >= 1)
            {
                return 4;
            }
            if (Vector3.Dot(transform.forward, Vector3.up) >= 1)
            {
                return 1;
            }
            if (Vector3.Dot(-transform.right, Vector3.up) >= 1)
            {
                return 3;
            }
            if (Vector3.Dot(-transform.up, Vector3.up) >= 1)
            {
                return 2;
            }
            if (Vector3.Dot(-transform.forward, Vector3.up) >= 1)
            {
                return 6;
            }
            if (Vector3.Dot(transform.up, Vector3.up) >= 1)
            {
                return 5;
            }

            return 1;
        }
    }

    public override int PlaneY
    {
        get
        {
            return 3;
        }
    }

    public override int DragSpeed
    {
        get
        {
            return 10;
        }
    }

    public override bool ShouldKeepLerpingWhenDropped
    {
        get
        {
            return false;
        }
    }
}
