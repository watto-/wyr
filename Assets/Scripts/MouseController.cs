﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour {

	/* Y Position on which the object will move when dragged */
	float planeY = 1;

    /* Reset position set when picked up to return to if anything goes wrong */
    Vector3 originalPosition;

    /* Object currently being dragged */
    DraggableObject objectBeingHeld;

    /* Plane used to keep draggable objects at the same height */
	Plane dragPlane;

    /* Reusable Raycast object */
	Ray ray;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonUp(0))
            OnMouseUp();			

		if(Input.GetMouseButton(0))
			OnMouseDown();
	}

	void OnMouseUp() {
        if(objectBeingHeld != null)
        {
            if (objectBeingHeld.CanBeDropped())
            {
                objectBeingHeld.Drop();
            }
            else
            {
                objectBeingHeld.MoveTo(originalPosition);
            }

            objectBeingHeld = null;
        }
	}

	void OnMouseDown(){
        if (!objectBeingHeld)
        {
            RaycastHit hit;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {
                DraggableObject target = hit.collider.GetComponent<DraggableObject>();
                if (target != null)
                {
                    objectBeingHeld = target;
                    originalPosition = target.gameObject.transform.position;
                    objectBeingHeld.OnPickup();
                }
            }
        }
        else
        {
            dragPlane = new Plane(Vector3.up, Vector3.up * objectBeingHeld.PlaneY);
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float distance;
            if (dragPlane.Raycast(ray, out distance))
            {
                objectBeingHeld.MoveTo(ray.GetPoint(distance));
            }
        }
	}
}
