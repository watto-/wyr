﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpot : MonoBehaviour {

	private Vector3 slotPosition;
	private Character occupyingPlayer;


	public Vector3 SlotPosition {
		get { return slotPosition; }
		
	}
	public Character OccupyingPlayer {
		get { return this.occupyingPlayer; }
		set { this.occupyingPlayer = value; }
	}

	// Use this for initialization
	void Start () {
		this.slotPosition = this.transform.position;
	}
}
